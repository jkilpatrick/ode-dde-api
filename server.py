from flask_cors import CORS

from dde_api import app, models, auth

from sandman.model import activate

import warnings
from sqlalchemy import exc as sa_exc

# Uncomment to get debug info
# import logging
# logging.basicConfig()
# logging.getLogger('sqlalchemy.orm.mapper').setLevel(logging.DEBUG)

with warnings.catch_warnings():
    warnings.simplefilter("ignore", category=sa_exc.SAWarning)
    activate(browser=False)

models.initialize_models()
CORS(app)

@auth.verify_password
def verify_password(login_id, password):
    user = models.User.find_by_login_id(login_id)

    if user is None or not user.check_password(password):
         return False

    return True

@app.before_request
# @auth.login_required
def before_request():
    pass

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5111, debug=True)
