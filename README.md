Setup
=====

Setup python env:

/usr/local/python3.4/bin/pyvenv python
export PATH=/usr/pgsql-9.2/bin:$PATH

To install the package:

    pip install -r requirements.txt
    python setup.py develop

To run tests:

    python setup.py test

To run the server:

    source python/bin/activate
    python server.py
