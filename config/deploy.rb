set :scm, :git
set :keep_releases, 5
set :product, 'dde'
set :application, 'ode-dde-api'
set :application_version, '0.1'
set :use_sudo, false
set :repo_url, 'git@rtkgit.pdx.rentrak.com:ode-dde-api.git'
set :branch, 'master'
set :deploy_to, "/data_storage/apps/#{fetch(:product)}/#{fetch(:application)}/#{fetch(:application_version)}"
set :deploy_via, :copy

namespace :deploy do

  desc "Install ODE DDE Metadata API App"
  task :install_python_package do
    on roles(:web) do
      execute "cd '#{release_path}'; /usr/bin/virtualenv34 -p /usr/bin/python34 env"
      execute "cd '#{release_path}'; ./env/bin/python setup.py install"
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:web), in: :sequence, wait: 5 do
      execute 'sudo /usr/bin/supervisorctl restart ode_dde_api_v0.1:*'
    end
  end

  after :updated, :install_python_package
  after :publishing, :restart

end
