from setuptools import setup

setup(
    name = "DDE-API",
    version = "0.1",
    packages = ["dde_api"],
    install_requires = [
        "Flask >= 0.10.1",
        "Flask-Admin >= 1.0",
        "Flask-SQLAlchemy >= 1.0",
        "Flask-RESTful >= 0.2.12",
        "Flask-Cors >= 1.6.1",
        "Flask-HTTPAuth >= 2.2.1",
        "Psycopg2 >= 2.5.2",
        "sandman >= 0.9.8",
        "itsdangerous >= 0.24",
        "py-bcrypt >= 0.4",
        "passlib >= 1.6.2"
    ],
    test_suite = "dde_api.test.run_tests.full_suite"
)
