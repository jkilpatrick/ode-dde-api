class Config(object):
    ENV='NONE'
    DEBUG=True
    SQLALCHEMY_DATABASE_URI='postgresql+psycopg2://digi_download:HnkP5SHdY@voddevprivatedb1:5439/digi_download_dev'
    SANDMAN_SHOW_PKS=True
    SECRET_KEY='00dd7a8f86768a249f21c836531e93f037c61a98'
    CORS_HEADERS='Content-Type'

class LiveConfig(Config):
    ENV='LIVE',
    DEBUG=False

class StageConfig(Config):
    ENV='STAGE'

class DevConfig(Config):
    ENV='DEV'

class TestConfig(Config):
    ENV='TEST'

settings_map = {
    'TEST' : DevConfig,
    'DEV'  : DevConfig,
    'STAGE': StageConfig,
    'LIVE' : LiveConfig
}
