# data_sources?data_source_no=:id
# data_interfaces?data_interface_no=:id

from dde_api import app, db
from dde_api.rtk_model import RtkModel

from sandman.model import register, Model
from sqlalchemy.orm.exc import NoResultFound
from passlib.hash import ldap_salted_sha1

import re

models = []
def add_class_for_tablename(tablename):
    p = re.compile(r's$')
    cls_name = ''.join([ p.sub('', word.capitalize()) for word in tablename.split('_') ])
    cls = type(cls_name, (Model,), dict(__tablename__ = tablename))
    models.append(cls)

_tablenames = (
    'apple_region_codes',
    'apple_unavailable_files',
    'apple_vendors',
    'apple_vendor_region_codes_whitelist',
    'data_sources',
    'data_interfaces',
    'distributors',
    'import_controls',
    'import_control_options',
    'licensees',
    'promo_type_xrefs',
)

for tablename in _tablenames:
    add_class_for_tablename(tablename)

class User(db.Model, RtkModel):
    __tablename__ = 'ua_users'

    user_no = db.Column(db.Integer, primary_key=True)
    login_id = db.Column(db.String(64))
    password = db.Column(db.String(64))

    @classmethod
    def find_by_login_id(cls, login_id):
        try:
            user = cls.query.filter(cls.login_id == login_id).one()
        except NoResultFound:
            user = None
        return user

    def check_password(self, password):
        ldap_salted_sha1.max_salt_size = 20
        return ldap_salted_sha1.verify(password, self.password)

register(models)

def initialize_models():
    pass
