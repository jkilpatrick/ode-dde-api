#!/usr/bin/env python

import argparse, unittest

def unit_suite():
    return unittest.TestLoader().discover('test', pattern='*.py', top_level_dir='dde_api')

def integration_suite():
    loader = unittest.TestLoader()
    loader.testMethodPrefix = 'integration_test'
    return loader.discover('test', pattern='*.py', top_level_dir='dde_api')

def full_suite():
    suite = unit_suite()
    suite.addTest(integration_suite())
    return suite

def single_test(test):
    loader = unittest.TestLoader()
    loader.testMethodPrefix = test
    return loader.discover('test', pattern='*.py', top_level_dir='dde_api')

def run_suite():
    parser = argparse.ArgumentParser(description='run tests')
    parser.add_argument('-u', '--unit', action='store_true', help='Only run unit tests')
    parser.add_argument('-i', '--integration', action='store_true', help='Only run integration tests')
    parser.add_argument('-v', '--verbosity', type=int, default=1, help='Verbosity level (default is 1)')
    parser.add_argument('-t', '--test', type=str, help='Run tests starting with ...')

    args = parser.parse_args()

    if args.unit:
        suite = unit_suite()
    elif args.integration:
        suite = integration_suite()
    elif args.test:
        suite = single_test(args.test)
    else:
        suite = full_suite()

    runner = unittest.TextTestRunner(verbosity=args.verbosity)
    runner.run(suite)

if __name__ == "__main__":
    run_suite()
