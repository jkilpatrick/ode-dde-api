from passlib.hash import ldap_salted_sha1
from dde_api import models
from dde_api.test.rtk_testcase import RtkTestCase

class UserModelTestCase(RtkTestCase):

    def test_insert_user(self):
        q = models.User.query.all()
        self.assertEqual(len(q), 1)

        self.insert_row(
            models.User(
                user_no=2,
                login_id='user_name2',
                password='pass',
            )
        )

        q = models.User.query.all()
        self.assertEqual(len(q), 2)

    def test_check_password_sha1(self):
        self.insert_row(
            models.User(
                login_id='user_name2',
                password= ldap_salted_sha1.encrypt('foo')
            )
        )
        self.assertTrue(models.User.find(2).check_password('foo'))

    def test_check_bad_password_sha1(self):
        self.insert_row(
            models.User(
                login_id='user_name2',
                password=ldap_salted_sha1.encrypt('foo')
            )
        )

        self.assertFalse(models.User.find(2).check_password('nofoo'))
