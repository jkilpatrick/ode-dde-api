import json, unittest
from base64 import b64encode
from logging import WARNING
from unittest.mock import Mock

from dde_api import app, db
from dde_api import models

class RtkTestCase(unittest.TestCase):

    def setUp(self):
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
        app.config['TESTING'] = True
        app.logger.setLevel(WARNING)
        self.app = app.test_client()
        self.maxDiff = None
        db.create_all()
        self.create_test_user()

    def create_test_user(self):
        self.insert_test_data(
            models.User,
            ('login_id', 'password', 'user_no'),
            [('test_user', 'abc123', 1)]
        )

    def login(self):
        real = models.User
        real.check_password = Mock(return_value=True)
        header = 'Basic ' + b64encode(b'test_user:abc123').decode('utf-8')
        response = self.app.post('/login', headers={'Authorization': header})
        obj = json.loads(response.data.decode('utf-8'))
        self.auth_token = obj['token']
        return response

    def logout(self):
        self.auth_token = None

    def auth_get(self, *args, **kw):
        header = 'Basic ' + b64encode(bytes(self.auth_token, 'utf-8') + b':unused').decode('utf-8')
        return self.app.get(*args, headers={'Authorization': header}, **kw)

    def auth_put(self, *args, **kw):
        header = 'Basic ' + b64encode(bytes(self.auth_token, 'utf-8') + b':unused').decode('utf-8')
        return self.app.put(*args, headers={'Authorization': header}, **kw)

    def auth_post(self, *args, **kw):
        header = 'Basic ' + b64encode(bytes(self.auth_token, 'utf-8') + b':unused').decode('utf-8')
        return self.app.post(*args, headers={'Authorization': header}, **kw)

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def insert_row(self, model_item):
        db.session.add(model_item)
        db.session.commit()

    def insert_rows(self, model_items):
        for i in model_items:
            self.insert_row(i)

    def assert_records_equals(self, records, fields, values):
        self.assertEqual(len(records), len(values))
        for i in range(len(records)):
            self.assert_record_equals(i, records[i], fields, values[i])

    def assert_record_equals(self, index, record, fields, values):
        for i in range(len(fields)):
            field = fields[i]
            value = self._value_for_test(field, values[i])
            record_value = getattr(record, field)
            self.assertEqual(record_value, value, '%s != %s (%s, record #%d)' % (record_value, value, field, index + 1))

    def assert_dicts_equals(self, records, fields, values):
        self.assertEqual(len(records), len(values))
        for i in range(len(records)):
            self.assert_dict_equals(records[i], fields, values[i])

    def assert_dict_equals(self, record, fields, values):
        for i in range(len(fields)):
            field = fields[i]
            value = self._value_for_test(field, values[i])
            record_value = record.get(field)
            self.assertEqual(record_value, value)

    def insert_test_data(self, cls, field_list, values_list):
        for values in values_list:

            kwargs = {}
            i = 0
            for value in values:

                field = field_list[i]
                value = self._value_for_test(field, value)
                kwargs[field] = value
                i = i + 1

            obj = cls(**kwargs)
            self.insert_row(obj)

    def _value_for_test(self, field, value):
        return value
