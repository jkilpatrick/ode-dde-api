from sandman import app
from flask.ext.httpauth import HTTPBasicAuth
from flask.ext.sqlalchemy import SQLAlchemy
from . import settings
import os

app.secret_key = 'Kartner'

env = os.environ.get('RTK_ENV')
app.config.from_object(settings.settings_map.get(env, settings.DevConfig))
app.config.from_envvar('DDE_API_CONFIG', silent=True)

db = SQLAlchemy(app)

auth = HTTPBasicAuth()
