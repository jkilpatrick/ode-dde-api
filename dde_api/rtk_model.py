from dde_api import db

class RtkModel:

    def __init__(self, **values):
        for field in list(values.keys()):
            setattr(self, field, values[field])

    @classmethod
    def find(cls, object_no):
        return cls.query.get(int(object_no))
